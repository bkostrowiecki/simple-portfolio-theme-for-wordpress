		<div class="footer-container">
			<div class="container fade-footer">
				<footer class="page-footer col-lg-12">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-left">
							Copyright <?php echo date('Y'); ?> &copy; <a class="link link--footer link--inner" href="<?php echo home_url(); ?>" title="<?php bloginfo( 'name' );?>"><?php echo bloginfo( 'name' ); ?></a>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 hidden-xs text-right">
							<a class="link link--footer" href="mailto:<?php bloginfo('admin_email'); ?>" title="Link for everyone who wants to send me an email"><?php bloginfo('admin_email'); ?></a>
						</div>
					</div>
				</footer>
			</div>
		</div>
		<?php wp_footer(); ?>
	</body>
</html>