<?php

function hexToRgb( $hex ) {
	$hex = str_replace( "#", "", $hex );

	if ( strlen( $hex ) == 3 ) {
		$r = hexdec( substr( $hex, 0, 1 ).substr( $hex, 0, 1 ) );
		$g = hexdec( substr( $hex, 1, 1 ).substr( $hex, 1, 1 ) );
		$b = hexdec( substr( $hex, 2, 1 ).substr( $hex, 2, 1 ) );
	} else {
		$r = hexdec( substr( $hex, 0, 2) );
		$g = hexdec( substr( $hex, 2, 2) );
		$b = hexdec( substr( $hex, 4, 2) );
	}
	$rgb = array( 'r' => $r, 'g' => $g, 'b' => $b );
	//return implode(",", $rgb); // returns the rgb values separated by commas
	return $rgb; // returns an array with the rgb values
}

/** TOP MENU RENDERING **/
function main_menu( $menu_name = null ) {
	if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) :

		$menu = wp_get_nav_menu_object( $locations[$menu_name] );
		$menu_items = wp_get_nav_menu_items( $menu->term_id );
		$menu_items = array_reverse ( $menu_items );
		
		$uri = 'http://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];

		$bg = get_option( 'menu_bg' );

		?>
		
		<ul id="menu-<?php echo $menu_name ?>" class="nav list list--nav col-xs-12 col-sm-12 col-md-12 col-lg-12 pull-right">
			<?php $i=0;
			foreach ( (array) $menu_items as $key => $menu_item ) : $i++; ?>
			<li class="list--nav__elem col-xs-12 col-sm-auto col-md-auto col-lg-auto pull-right <?php if ($uri===$menu_item->url) { echo 'active '; } ?>">
				<a class="link link--inner link--nav <?php if ($uri===$menu_item->url) { echo 'active '; } ?>" href="<?php echo $menu_item->url ?>" title="<?php echo $menu_item->attr_title; ?>" ><?php echo $menu_item->title; ?></a>
				<?php if ($uri===$menu_item->url) : ?> <span class="testexp pointer">&nbsp;</span> <?php endif; ?>
			</li>
			<?php endforeach; ?>
		</ul>

	<?php endif;
}
/** End of TOP MENU RENDERING **/

/** SHORTCODE FUNCTIONS **/
function portfolio_all_tags() {
	$content = '<div class="content--single__tags">';
	$tags = get_terms('post_tag');
  foreach ( $tags as $key => $tag ) {
		if ( 'edit' == 'view' )
			$link = get_edit_tag_link( $tag->term_id, 'post_tag' );
		else
			$link = get_term_link( intval($tag->term_id), 'post_tag' );
		if ( is_wp_error( $link ) )
			return false;

		$content .= '<a href="'.$link.'" rel="tag" class="link link--inner">'.$tag->name.'</a>'; 
	}
	$content .= '</div>';
	return $content;
}
add_shortcode( 'all_tags', 'portfolio_all_tags' );

/** End of SHORTCODE FUNCTIONS **/

/** Some not so smart functions **/
function new_excerpt_length($length) {
	return 50;
}
/** End of Some not so smart functions **/

/** MAIN THEME FUNCTIONS **/
function portfolio_register_menus() {
	register_nav_menus( 
		array (
			'main-menu' => __('Main menu', 'simple-portfolio-theme')
		)
	);
}

function portfolio_theme_setup() {
	add_theme_support( 'custom-header' );
	add_theme_support( 'post-thumbnails' );
}

function portfolio_customize_preview() {
	?>
	<script type="text/javascript">
	( function( $ ){
		wp.customize('main_bg', function( value ) {
			value.bind( function( to ) {
				$('.page-content--default').css( 'background', to );
			});
		});
		wp.customize('menu_bg', function( value ) {
			value.bind( function( to ) {
				$('.header-container').css( 'background', to );
			});
		});
		wp.customize('footer_bg', function( value ) {
			value.bind( function( to ) {
				$('body').css( 'background', to );
				$('.footer-container').css( 'background', to );
			});
		});

		wp.customize('normal_menu_clr', function( value ) {
			value.bind( function( to ) {
				$('.link--nav').css( 'color', to );
			});
		});
		wp.customize('hover_menu_clr', function( value ) {
			value.bind( function( to ) {
				$('#customization').append('.link--nav:hover { color: ' + to + ' !important; } ');
			});
		});
		wp.customize('active_menu_clr', function( value ) {
			value.bind( function( to ) {
				$('.list--nav__elem > .active').css( 'color', to );
			});
		});

		wp.customize('main_font_clr', function( value ) {
			value.bind( function( to ) {
				$('#customization').append('.page-content { color: ' + to + ' !important; } ');
				$('#customization').append('.heading .heading--breadcrump { color: ' + to + ' !important; } ');
			});
		});
		wp.customize('title_font_clr', function( value ) {
			value.bind( function( to ) {
				$('#customization').append('.heading--name { color: ' + to + ' !important; } ');
			});
		});
		wp.customize('footer_font_clr', function( value ) {
			value.bind( function( to ) {
				$('#customization').append('.page-footer { color: ' + to + ' !important; } ');
			});
		});
		wp.customize('footer_link_clr', function( value ) {
			value.bind( function( to ) {
				$('#customization').append('.link--footer  { color: ' + to + ' !important; } ');
			});
		});
		wp.customize('footer_hover_link_clr', function( value ) {
			value.bind( function( to ) {
				$('#customization').append('.link--footer:hover  { color: ' + to + ' !important; } ');
			});
		});

		wp.customize('tile_font_clr', function( value ) {
			value.bind( function( to ) {
				$('#customization').append('.content--tile { color: ' + to + ' !important; } ');
			});
		});
		wp.customize('tile_tag_font_clr', function( value ) {
			value.bind( function( to ) {
				$('#customization').append('.content--tile__tags > a { color: ' + to + ' !important; } ');
			});
		});
		wp.customize('tile_bg', function( value ) {
			value.bind( function( to ) {
				var patt = /^#([\da-fA-F]{2})([\da-fA-F]{2})([\da-fA-F]{2})$/;
				var matches = patt.exec(to);
				var rgba = "rgba("+parseInt(matches[1], 16)+","+parseInt(matches[2], 16)+","+parseInt(matches[3], 16)+","+parseFloat(0.9)+");";
				var rgbaNorm = "rgba("+parseInt(matches[1], 16)+","+parseInt(matches[2], 16)+","+parseInt(matches[3], 16)+","+parseFloat(0.0)+");" 
				$('#customization').append('.art--tile:hover > .content--tile { background: ' + rgba + ' !important; } ');
				$('#customization').append('.content--tile { background: ' + rgbaNorm + ' !important; } ');
			});
		});
		wp.customize('tile_tag_bg', function( value ) {
			value.bind( function( to ) {
				$('#customization').append('.content--tile__tags > a { background: ' + to + ' !important; } ');
			});
		});
	} )( jQuery )
	</script>
	<?php 
}

function portfolio_customize_register( $wp_customize ) {
	$wp_customize->add_section( 'main_look_stgs', array(
		'title'          => __( 'Main look settings', 'simple-portfolio-theme' ),
		'priority'       => 35,
	) );
	$wp_customize->add_setting( 'main_font_clr', array(
		'default'        => '#ffffff',
		'type'           => 'option',
		'capability'     => 'edit_theme_options',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'main_font_clr_ctrl', array(
		'settings' => 'main_font_clr',
		'label'    => __( 'Main font color', 'simple-portfolio-theme' ),
		'section'  => 'main_look_stgs',
		// 'type'     => 'text',
	) ) );
	$wp_customize->add_setting( 'main_bg', array(
		'default'        => '#1a76c4',
		'type'           => 'option',
		'capability'     => 'edit_theme_options',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'main_bg_ctrl', array(
		'settings' => 'main_bg',
		'label'    => __( 'Main background color', 'simple-portfolio-theme' ),
		'section'  => 'main_look_stgs',
		// 'type'     => 'text',
	) ) );

	/** BACKGROUNDS CUSTOMIZATION **/
	// Main background
	
	/** MENU COLORS CUSTOMIZATION **/
	$wp_customize->add_section( 'menu_look_stgs', array(
		'title'          => __( 'Menu look settings', 'simple-portfolio-theme' ),
		'priority'       => 36,
	) );
	$wp_customize->add_setting( 'menu_bg', array(
		'default'        => '#ffffff',
		'type'           => 'option',
		'capability'     => 'edit_theme_options',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'menu_bg_ctrl', array(
		'settings' => 'menu_bg',
		'label'    => __( 'Menu background color', 'simple-portfolio-theme' ),
		'section'  => 'menu_look_stgs',
		// 'type'     => 'text',
	) ) );

	$wp_customize->add_setting( 'normal_menu_clr', array(
		'default'        => '#c3c3c3',
		'type'           => 'option',
		'capability'     => 'edit_theme_options',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'normal_menu_clr_ctrl', array(
		'settings' => 'normal_menu_clr',
		'label'    => __( 'Menu link color', 'simple-portfolio-theme' ),
		'section'  => 'menu_look_stgs',
		// 'type'     => 'text',
	) ) );

	$wp_customize->add_setting( 'hover_menu_clr', array(
		'default'        => '#ef7a40',
		'type'           => 'option',
		'capability'     => 'edit_theme_options',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hover_menu_clr_ctrl', array(
		'settings' => 'hover_menu_clr',
		'label'    => __( 'Hover menu link color', 'simple-portfolio-theme' ),
		'section'  => 'menu_look_stgs',
		// 'type'     => 'text',
	) ) );

	$wp_customize->add_setting( 'active_menu_clr', array(
		'default'        => '#000000',
		'type'           => 'option',
		'capability'     => 'edit_theme_options',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'active_menu_clr_ctrl', array(
		'settings' => 'active_menu_clr',
		'label'    => __( 'Active menu link color', 'simple-portfolio-theme' ),
		'section'  => 'menu_look_stgs',
		// 'type'     => 'text',
	) ) );
	/** End of MENU COLORS CUSTOMIZATION **/

	$wp_customize->add_section( 'footer_look_stgs', array(
		'title'          => __( 'Footer look settings', 'simple-portfolio-theme' ),
		'priority'       => 35,
	) );
	// Footer background
	$wp_customize->add_setting( 'footer_bg', array(
		'default'        => '#ffffff',
		'type'           => 'option',
		'capability'     => 'edit_theme_options',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_bg_ctrl', array(
		'settings' => 'footer_bg',
		'label'    => __( 'Footer background color', 'simple-portfolio-theme' ),
		'section'  => 'footer_look_stgs',
		// 'type'     => 'text',
	) ) );

	$wp_customize->add_setting( 'footer_font_clr', array(
		'default'        => '#ffffff',
		'type'           => 'option',
		'capability'     => 'edit_theme_options',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_font_clr_ctrl', array(
		'settings' => 'footer_font_clr',
		'label'    => __( 'Footer hover link color', 'simple-portfolio-theme' ),
		'section'  => 'footer_look_stgs',
		// 'type'     => 'text',
	) ) );

	$wp_customize->add_setting( 'footer_link_clr', array(
		'default'        => '#939393',
		'type'           => 'option',
		'capability'     => 'edit_theme_options',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_link_clr_ctrl', array(
		'settings' => 'footer_link_clr',
		'label'    => __( 'Footer font color', 'simple-portfolio-theme' ),
		'section'  => 'footer_look_stgs',
		// 'type'     => 'text',
	) ) );

	$wp_customize->add_setting( 'footer_hover_link_clr', array(
		'default'        => '#ef7a40',
		'type'           => 'option',
		'capability'     => 'edit_theme_options',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_hover_link_clr_ctrl', array(
		'settings' => 'footer_hover_link_clr',
		'label'    => __( 'Footer hover font color', 'simple-portfolio-theme' ),
		'section'  => 'footer_look_stgs',
		// 'type'     => 'text',
	) ) );
	/** End of BACKGROUNDS CUSTOMIZATIONS **/

	
	$wp_customize->add_section( 'title_look_stgs', array(
		'title'          => __( 'Title look settings', 'simple-portfolio-theme' ),
		'priority'       => 30,
	) );
	/** FONT COLOR CUSTOMIZATION **/
	$wp_customize->add_setting( 'title_font_clr', array(
		'default'        => '#000000',
		'type'           => 'option',
		'capability'     => 'edit_theme_options',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'title_font_clr_ctrl', array(
		'settings' => 'title_font_clr',
		'label'    => __( 'Title font color', 'simple-portfolio-theme' ),
		'section'  => 'title_look_stgs',
		// 'type'     => 'text',
	) ) );

	/** End of FONT COLOR CUSTOMIZATION **/

	/** TILE CUSTOMIZATION **/
	$wp_customize->add_section( 'tile_look_stgs', array(
		'title'          => __( 'Tile look settings', 'simple-portfolio-theme' ),
		'priority'       => 38,
	) );

	$wp_customize->add_setting( 'tile_bg', array(
		'default'        => '#d58100',
		'type'           => 'option',
		'capability'     => 'edit_theme_options',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'tile_bg_ctrl', array(
		'settings' => 'tile_bg',
		'label'    => __( 'Tile background color', 'simple-portfolio-theme' ),
		'section'  => 'tile_look_stgs',
		// 'type'     => 'text',
	) ) );

	$wp_customize->add_setting( 'tile_tag_bg', array(
		'default'        => '#1a76c4',
		'type'           => 'option',
		'capability'     => 'edit_theme_options',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'tile_tag_bg_ctrl', array(
		'settings' => 'tile_tag_bg',
		'label'    => __( 'Tile tag background color', 'simple-portfolio-theme' ),
		'section'  => 'tile_look_stgs',
		// 'type'     => 'text',
	) ) );

	$wp_customize->add_setting( 'tile_font_clr', array(
		'default'        => '#ffffff',
		'type'           => 'option',
		'capability'     => 'edit_theme_options',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'tile_font_clr_ctrl', array(
		'settings' => 'tile_font_clr',
		'label'    => __( 'Tile font color', 'simple-portfolio-theme' ),
		'section'  => 'tile_look_stgs',
		// 'type'     => 'text',
	) ) );

	$wp_customize->add_setting( 'tile_tag_font_clr', array(
		'default'        => '#ffffff',
		'type'           => 'option',
		'capability'     => 'edit_theme_options',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'tile_tag_font_clr_ctrl', array(
		'settings' => 'tile_tag_font_clr',
		'label'    => __( 'Tile tag font color', 'simple-portfolio-theme' ),
		'section'  => 'tile_look_stgs',
		// 'type'     => 'text',
	) ) );

	/** End of TILE CUSTOMIZATION **/

	if ( $wp_customize->is_preview() && ! is_admin() ) {
		add_action( 'wp_footer', 'portfolio_customize_preview', 21);
	}

	$wp_customize->get_setting('main_bg')->transport = 'postMessage';
	$wp_customize->get_setting('menu_bg')->transport = 'postMessage';
	$wp_customize->get_setting('footer_bg')->transport = 'postMessage';

	$wp_customize->get_setting('normal_menu_clr')->transport = 'postMessage';
	$wp_customize->get_setting('hover_menu_clr')->transport = 'postMessage';
	$wp_customize->get_setting('active_menu_clr')->transport = 'postMessage';

	$wp_customize->get_setting('main_font_clr')->transport = 'postMessage';
	$wp_customize->get_setting('footer_font_clr')->transport = 'postMessage';
	$wp_customize->get_setting('title_font_clr')->transport = 'postMessage';
	$wp_customize->get_setting('footer_link_clr')->transport = 'postMessage';
	$wp_customize->get_setting('footer_hover_link_clr')->transport = 'postMessage';

	$wp_customize->get_setting('tile_bg')->transport = 'postMessage';
	$wp_customize->get_setting('tile_tag_bg')->transport = 'postMessage';
	$wp_customize->get_setting('tile_font_clr')->transport = 'postMessage';
	$wp_customize->get_setting('tile_tag_font_clr')->transport = 'postMessage';
}

function portfolio_clean_sprites( $path ) {
	$colors = array();
	$query = new WP_Query( array( 'post_type' => 'page' ) );
	while ( $query->have_posts() ) {
		$query->the_post();
		$value = get_post_meta( get_the_ID(), "background", true );
		array_push( $colors, substr($value, 1) );
	}
	array_push( $colors, substr( get_option( 'main_bg' ), 1 ) );

	$dir = dir( $path );
	$trigger = false;
	while ( $file = $dir->read() ) {
		$trigger = false;
		foreach ( $colors as $color ) {
			if ( strpos( $file, $color )!==false ) {
				$trigger = true;
			} else if ( $file === '.' || $file === '..' ) {
				$trigger = true;
			}
		}
		if ( $trigger===false ) {
			unlink( $path.$file );
		}
	}
}

function portfolio_customize_save_after() {
	require_once( 'spritegen.php' );

	$color = get_option( 'main_bg' );
	$colorString = substr( $color, 1 );

	$emitter = new SpriteGenerator( 51, 26, 1 );
	$emitter->setWorkingFrame( 0 );
	$emitter->generateTriangle( hexToRgb( $color ) );

	$emitter->saveToPng( '../wp-content/themes/simpleportfolio/img/testexp-'.$colorString.'.png' );

	portfolio_clean_sprites('../wp-content/themes/simpleportfolio/img/');
}

function portfolio_updated_post( $post_id ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
				return;

	require_once( 'spritegen.php' );

	$color = get_post_meta( $post_id, 'background', true );
	$colorString = substr( $color, 1 );

	$emitter = new SpriteGenerator( 51, 26, 1 );
	$emitter->setWorkingFrame( 0 );
	$emitter->generateTriangle( hexToRgb( $color ) );

	$emitter->saveToPng( '../wp-content/themes/simpleportfolio/img/testexp-'.$colorString.'.png' );

	portfolio_clean_sprites('../wp-content/themes/simpleportfolio/img/');
}

function portfolio_load_theme_textdomain() {
	load_theme_textdomain('simple-portfolio-theme', get_template_directory() . '/lang');
}

function portfolio_scriptloader() {
	// wp_enqueue_script( 'bootstrap', get_template_directory_uri().'/lib/js/bootstrap.min.js', array( 'jquery' ), '4.0.6', true );
	wp_enqueue_script( 'simple-portoflio-theme', get_template_directory_uri().'/js/simpleportfolio.js', array( 'jquery' ), '', true );
}

function portfolio_enqueue_script() {
	//wp_register_script( 'jquery-2.1.1', 'http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js', true, '2.1.1' );
	//wp_enqueue_script('jquery-2.1.1');
}

function portfolio_adminhead() {
	?>
	<script>
		templateUrl = '<?php echo get_bloginfo("template_url"); ?>';
	</script>
	<?php
}

function portfolio_tags() {
	$posttags = get_the_tags();
	if ($posttags) {
	  foreach($posttags as $tag) {
	    echo '<a href="'.get_tag_link( $tag->term_id ).'" rel="tag" class="link link--inner">'.$tag->name.'</a>'; 
	  }
	}
}

/** END MAIN THEME FUNCTIONS **/

add_action( 'init', 'portfolio_theme_setup');
add_action( 'init', 'portfolio_register_menus' );
// add_action( 'init', 'portfolio_add_shortcodes');
add_action( 'init', 'portfolio_load_theme_textdomain' );
add_action( 'init', 'portfolio_scriptloader' );
add_action( 'wp_enqueue_scripts', 'portfolio_enqueue_script' );

add_action( 'admin_head', 'portfolio_adminhead' );
add_action( 'portfolio_tags', 'portfolio_tags' );

add_action( 'save_post', 'portfolio_updated_post', 20, 1 );
add_action( 'customize_register', 'portfolio_customize_register' );
add_action( 'customize_save_after', 'portfolio_customize_save_after' );

add_filter( 'excerpt_length', 'new_excerpt_length' );