<!doctype html>
<html <?php language_attributes(); ?> >
	<head>
		<meta chaset="<?php bloginfo( 'charset' ); ?> ">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php wp_title( ' - ', true, 'right');?></title>
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/lib/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/lib/css/fix-annoying-bootstrap.css">
		<!-- Fix for Internet Explorer prior to version 9 by Remy Sharp http://remysharp.com/2009/01/07/html5-enabling-script/ -->
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!--[if lte IE 7]> <script src="js/IE8.js" type="text/javascript"></script><! [endif]--> <!--[if lt IE 7] --> 
		
		<?php wp_head(); ?>

		<script type="text/javascript">
			templateUrl = '<?php echo get_bloginfo("template_url"); ?>';
		</script>

		<style id="customization">
			body {
				background: <?php echo get_option( 'footer_bg' ); ?> !important;
			}

			.link--nav {
				color: <?php echo get_option( 'normal_menu_clr' ); ?> !important;
				background: <?php echo get_option( 'menu_bg' ); ?> !important;
			}

			.link--nav:hover {
				color: <?php echo get_option( 'hover_menu_clr' ); ?> !important;
				background: <?php echo get_option( 'menu_bg' ); ?> !important;
			}

			.list--nav__elem > .active {
				color: <?php echo get_option( 'active_menu_clr' ); ?> !important;
				background: <?php echo get_option( 'menu_bg' ); ?> !important;
			}

			.content-container {
				color: <?php echo get_option( 'main_font_clr' ); ?> !important;
				background: <?php echo get_option( 'main_bg' ); ?>;
			}

			.header-container {
				background: <?php echo get_option( 'menu_bg' ); ?> !important;
			}

			.footer-container {
				color: <?php echo get_option( 'footer_font_clr' ); ?> !important;
				background: <?php echo get_option ( 'footer_bg '); ?> !important;
			}

			.link--footer {
				color: <?php echo get_option( 'footer_link_clr' ); ?> !important;
			}

			.link--footer:hover {
				color: <?php echo get_option( 'footer_hover_link_clr' ); ?> !important;
			}

			.heading--name {
				color: <?php echo get_option( 'title_font_clr' ); ?> !important;
			}

			.heading--breadcrump, .content {
				color: <?php echo get_option( 'main_font_clr' ); ?> !important;
			}

			<?php 
				$rgb = hexToRgb( get_option( 'tile_bg' ) );
			?>

			@media (max-width: 767px) { 
				.content--tile {
					background: rgba(<?php echo $rgb['r'];?>, <?php echo $rgb['g'];?>, <?php echo $rgb['b'];?>, 0.9);
				}
			}
			@media (min-width: 768px) {
				.content--tile {
					background: rgba(<?php echo $rgb['r'];?>, <?php echo $rgb['g'];?>, <?php echo $rgb['b'];?>, 0.0);
				}
				.art--tile:hover .content--tile {
					color: <?php echo get_option( 'tile_font_clr' );?> !important;
					background: rgba(<?php echo $rgb['r'];?>, <?php echo $rgb['g'];?>, <?php echo $rgb['b'];?>, 0.9);
				}
			}

			.content--tile__tags > a {
				color: <?php echo get_option( 'tile_tag_font_clr' );?> !important;
				background: <?php echo get_option( 'tile_tag_bg' );?>;
			}

			.page-content {
				color: <?php echo get_option( 'main_font_clr' ); ?> !important;
			}
		</style>

		<style id="no-js">
			/** FADE IN MECHANISM **/
			.fade-content {
				display: block;
			}
			
			.fade-footer {
				display: block;
			}
			
			/** End of FADE IN MECHANISM **/
		</style>

		<script>
			document.getElementById('no-js').innerHTML = '';
		</script>

	</head>

	<body <?php body_class('development'); ?> >
		<div class="header-container">
			<div class="container">
				<header class="row page-header">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 page-headings">
						<div class="page-headings-wrapper">
						<?php $header_image = get_header_image();
						if ( !empty( $header_image ) ) : ?>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
								<img src="<?php echo esc_url( $header_image ); ?>" class="img img--header img-circle" width="<?php echo get_custom_header()->	width; ?>" 	height="<?php echo get_custom_header()->height; ?>" alt="My avatar" />
							</a>
						<?php endif; 
							if ( is_home() || is_front_page() ) :
							?>
							<h1 class="heading heading--name"><?php bloginfo( 'name' ) ?></h1>
							<?php else: ?>
							<div class="heading heading--name"><?php bloginfo( 'name' ) ?></div>
							<?php endif; ?>
						</div>
					</div>
					<nav class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<?php main_menu('main-menu'); ?>
					</nav>
				</header>
			</div>
		</div>