var $j = jQuery.noConflict();

var rgbToHex = function( rgb ) {
	if (rgb === undefined || rgb === null) {
		return '';
	}
	rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
	if ( rgb === null ) { return '' };
	if ( rgb.length < 3 ) { return '' };
	return "#" +
		("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
		("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
		("0" + parseInt(rgb[3],10).toString(16)).slice(-2);
}

var Portfolio = function() {
	var mobileSize = false;
	this.content = jQuery('.fade-content');
	this.footer = jQuery('.fade-footer');
}

Portfolio.prototype.setPointer = function() {
	var pointer = jQuery('.list--nav__elem > .testexp');
	var color = this.content.css('background-color');
	color = rgbToHex( color );
	color = color.substr( 1 );
	pointer.css ( 'background-image', 'url( ' + templateUrl + '/img/testexp-' + color + '.png )' );
	pointer.css( 'margin-left', (parseFloat(jQuery('.list--nav__elem > .active').width()+30))/2 - parseFloat(50/2));
	pointer.fadeIn();
}

Portfolio.prototype.fades = function() {
	var tileAnimationFunc = function() {
		tiles.animate( {
		  width: [ "toggle", "swing" ],
		  height: [ "toggle", "swing" ],
		}, 500, "linear" );
	};

	if (this.mobileSize===false) {
		this.content.slideDown(500);
		this.footer.fadeIn(500);
	} else {
		this.content.show();
		this.footer.show();
	}	
}

Portfolio.prototype.registerEvents = function() {
	var cache = this;
	var footer = this.footer;
	var content = this.content;
	var linkFunc = function(e) {
		e.preventDefault();
		var href = this.getAttribute('href');
		if (cache.mobileSize===true) { location.href = href; return true; }
		jQuery('.pointer').fadeOut();
		footer.slideUp(500);
		content.slideUp(500, function() { 
			window.location.href = href; 
		});
	};

	jQuery('.link--inner').click( linkFunc );
	jQuery('.more-link').click( linkFunc );

	jQuery('.link--nav').resize( function() {
		cache.setPointer();
	});

	//jQuery(window).unload( function() {} );
}

Portfolio.prototype.checkWindowSize = function() {
	if ( jQuery(window).width() < 768 ) {
		this.mobileSize = true;
	}
	else {
		this.mobileSize = false;
	}
}

var portfolio = new Portfolio();

jQuery(document).ready( function() {
	portfolio.checkWindowSize();
	portfolio.registerEvents();
	portfolio.setPointer();
	portfolio.fades();
} );

jQuery(document).focus( function() {
	portfolio.checkWindowSize();
	portfolio.registerEvents();
	portfolio.setPointer();
	portfolio.fades();
} );

jQuery(document).resize( function() {
	portfolio.checkWindowSize();
	portfolio.setPointer();
} );