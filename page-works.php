<?php get_header();
	if ( have_posts() ) : 
		while ( have_posts() ) : the_post(); $bgcolor = get_post_meta( $post->ID, 'background', true ); 
		if ( $bgcolor == '' ) {
			$bgcolor = get_option( 'main_bg' );
		}
	?>
	<div class="bg-blue fade-content" style="background: <?php echo $bgcolor; ?>" >
		<?php endwhile; endif; ?>
		<div class="container">
			<?php wp_reset_query(); ?>
			<?php query_posts ( array( 'post_type' => 'post' ) ); ?>
			<?php $tag = single_tag_title( '<h1 class="heading heading--breadcrump"> '.__('Works made with ', 'simple-portfolio-theme').' ', false ); ?>
			<?php echo $tag.'</h1>'; ?>
			<?php
			if ( have_posts() ) : 
				?>
				<section class="page-content">
				<?php
				while ( have_posts() ) : the_post(); ?>
				<?php
				$thumbnail = '';
				// Get the ID of the post_thumbnail (if it exists)
				$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
				// if it exists
				if ( $post_thumbnail_id ) {
					$size = apply_filters( 'post_thumbnail_size', 'post-thumbnail' );
					$thumbnail = wp_get_attachment_image_src( $post_thumbnail_id, 'post-thumbnail', false );
				}
				global $more;
				$more = 0;
				?>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<article <?php post_class('art art--tile'); 
					if ( !empty( $thumbnail ) ) : ?> 
						style="background-image: url('<?php echo $thumbnail[0]; ?>')">
					<?php endif; ?>
						<div class="content content--tile col-lg-12">
							<h2 class="heading heading--tile" rel="entry-title"><a class="link link--inner" href="<?php the_permalink(); ?>" title="<?php the_title_attribute();?>"><?php the_title(); ?></a></h2>
							<?php $link = get_post_meta( $post->ID, "link", true ); 
							if ( $link!='' ) :?>
							<div class="content--tile__link-container">
								<span class="glyphicon glyphicon-link"></span>&nbsp;&nbsp;<a href="<?php echo $link; ?>" title="Link to <?php echo $link; ?>"><?php echo $link; ?></a>
							</div>
							<?php endif; ?>
							<div class="content--tile__excerpt hidden-xs">
								<?php the_content( __('Read more').'  <span class="glyphicon glyphicon-play glyphicon--readmore"></span>'); ?>
							</div>
							<div class="content--tile__tags">
								<?php do_action( 'portfolio_tags' ); ?>
							</div>
							<?php edit_post_link( __('Edit post', 'simple-portfolio-theme'), '', '' ); ?>
						</div>
					</article>
				</div>
			<?php endwhile; 
			?> </section> <?php
			else: ?>
			<article class="page-content">
				<h2><?php _e( "I am so sorry, but nothing's here!", 'simple-portfolio-theme'); ?></h2>
				<p><?php _e( "There are no matching results for your request.", 'simple-portfolio-theme'); ?></p>
			</article>
		<?php endif; ?>
		</div>
	</div>
<?php get_footer();