<?php get_header(); ?>
<?php
	$bgcolor = get_option( 'main_bg' );
	if ( have_posts() ) : 
		while ( have_posts() ) : the_post(); $bgcolor = get_post_meta( $post->ID, 'background', true ); 
		if ( $bgcolor == '' ) {
			$bgcolor = get_option( 'main_bg' );
		}?>
		<div class="content-container fade-content" style="background: <?php echo $bgcolor; ?> ">
			<div class="container">
				<div class="page-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<article <?php post_class('art art--page'); 
					if ( !empty( $thumbnail ) ) : ?> 
						style="background-image: url('<?php echo $thumbnail[0]; ?>')">
					<?php endif; ?>
						<div class="content content--page col-lg-12">
							<h1 class="heading heading--page"><?php the_title(); ?></h1>
							<div class="content--page__content">
								<?php the_content(); ?>
							</div>
							<?php edit_post_link( __('Edit page', 'simple-portfolio-theme'), '', '' ); ?>
						</div>
					</article>
				</div>
			</div>
		</div>
	<?php endwhile; 
	?> <?php
	else: ?>
		<div class="bg-blue fade-content" style="background: <?php echo $bgcolor; ?>">
			<div class="container">
				<div class="page-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<article class="page-content">
						<h2><?php _e( "I am so sorry, but nothing's here!", 'simple-portfolio-theme'); ?></h2>
						<p><?php _e( "There are no matching results for your request.", 'simple-portfolio-theme'); ?></p>
					</article>
				</div>
			</div>
		</div>
	<?php endif; ?>
<?php get_footer();