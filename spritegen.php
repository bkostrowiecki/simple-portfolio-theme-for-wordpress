<?php

class SpriteGenerator {
	protected $imgsize = array ( 'x' => 0, 'y' => 0 );
	protected $framesize = array ( 'x' => 0, 'y' => 0 );
	protected $image;
	protected $framesNum = 0;
	protected $workingFrame = 0;

	public function __construct( $width, $height, $frames ) {
		$this->imgsize['x'] = $width;
		$this->imgsize['y'] = $height * $frames;
		$this->framesize['x'] = $width;
		$this->framesize['y'] = $height;
		$this->framesNum = $frames;

		$this->image = imagecreatetruecolor( $this->imgsize['x'], $this->imgsize['y'] );

		$bg = imagecolorallocate( $this->image, 255, 0, 255 );
		imagecolortransparent ( $this->image , $bg );
		imagefilledrectangle( $this->image, 0, 0, $this->imgsize['x']-1, $this->imgsize['y']-1, $bg );
	}

	public function __destruct() {
		imagedestroy( $this->image );
	}

	public function setWorkingFrame( $numOfFrame ) {
		$this->workingFrame = $numOfFrame;
	}

	public function generateTriangle( array $color ) {
		$valuesForTriangle = array(
			($this->framesize['x']-1)/2,  $this->workingFrame * $this->framesize['y'],  // Point 1 (32, 9)
			0,  $this->workingFrame*$this->framesize['y'] + ($this->framesize['y']-1), // Point 2 (0 , 64)
			$this->framesize['x']-1,  $this->workingFrame*$this->framesize['y'] + ($this->framesize['y']-1)  // Point 3 (64, 64)
		);

		$colorHolder = imagecolorallocate( $this->image, $color['r'], $color['g'], $color['b'] );
		imagefilledpolygon( $this->image, $valuesForTriangle, 3, $colorHolder );
	}

	public function generateRectangle( array $color ) {
		$valuesForTriangle = array(
			$this->framesize['x']-1,  $this->workingFrame*$this->framesize['y'] -1,  // Point 0 (64, 0)
			0,  $this->workingFrame*$this->framesize['y'] -1,  // Point 1 (0, 0)
			0,  $this->workingFrame*$this->framesize['y'] + ($this->framesize['y']-1), // Point 2 (0 , 64)
			$this->framesize['x']-1,  $this->workingFrame*$this->framesize['y'] + ($this->framesize['y']-1)  // Point 3 (64, 64)
		);

		$colorHolder = imagecolorallocate( $this->image, $color['r'], $color['g'], $color['b'] );
		imagefilledpolygon( $this->image, $valuesForTriangle, 4, $colorHolder );
	}

	public function flush() {
		header( 'Content-type: image/png' );
		imagepng( $this->image );
	}

	public function saveToPng( $filename ) {
		imagepng( $this->image, $filename );
	}

	public function export( $cssname, $pngname, $spritename ) {
		$css = ".$spritename { background: no-repeat url('$pngname'); } \n";
		for ( $i=0; $i<$this->framesNum; $i++ ) {
			$bgimgY = $i*$this->framesize['y'];
			$css .= ".$spritename-frame$i { background-position: 0 $bgimgY; } \n"; 
		}
		$fp = fopen( $cssname, 'w' );
		fputs( $fp, $css );
		fclose( $fp );

		$this->saveToPng( $pngname );

		return true;
	}
}

// $test = new SpriteGenerator( 51, 26, 4 );
// $test->setWorkingFrame( 0 );
// $test->generateRectangle( hexToRgb('#ffffff') );
// $test->setWorkingFrame( 1 );
// $test->generateTriangle( hexToRgb('#1a76c4') );
// $test->setWorkingFrame( 2 );
// $test->generateTriangle( hexToRgb('#80ae00') );
// $test->setWorkingFrame( 3 );
// $test->generateTriangle( hexToRgb('#ff574f') );

// $test->export( 'testexp.css', 'testexp.png', 'testexp');